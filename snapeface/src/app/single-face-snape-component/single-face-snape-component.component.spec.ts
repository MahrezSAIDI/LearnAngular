import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleFaceSnapeComponentComponent } from './single-face-snape-component.component';

describe('SingleFaceSnapeComponentComponent', () => {
  let component: SingleFaceSnapeComponentComponent;
  let fixture: ComponentFixture<SingleFaceSnapeComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleFaceSnapeComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SingleFaceSnapeComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
